<footer class="bg-dark pt-5 text-light" data-bs-theme="dark">
    <div class="container">
        <!-- Row START -->
        <div class="row">
            <!-- Widget 1 START -->
            <div class="col-lg-3">
                <!-- logo -->
                <a href="/">
                    <img class="h-40px" src="{{ asset('/img/logo-light.svg') }}" width="150" alt="logo">
                </a>
                <p class="my-3 text-body-secondary">Лучшие мастера города и полный спектр услуг для вашего авто.</p>
                <p class="mb-2"><a href="tel:77772448909" class="text-body-secondary text-primary-hover"><i class="bi bi-telephone me-2"></i>+7 777 244 8909</a> </p>
                <p class="mb-0"><a href="mailto:recarservice@gmail.com" class="text-body-secondary text-primary-hover"><i class="bi bi-envelope me-2"></i>recarservices@gmail.com</a></p>
            </div>
            <!-- Widget 1 END -->

            <!-- Widget 2 START -->
            <div class="col-lg-8 ms-auto">
                <div class="row">
                    <!-- Link block -->
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <h5 class="text-white mb-2 mb-md-4">Компания</h5>
                        <ul class="nav flex-column text-primary-hover">
                            <li class="nav-item"><a class="nav-link text-body-secondary" href="#">О нас</a></li>
                            <li class="nav-item"><a class="nav-link text-body-secondary" href="{{ $googleFormLink }}" target="_blank">Для частных механиков</a></li>
                            <li class="nav-item"><a class="nav-link text-body-secondary" href="{{ $googleFormLink }}" target="_blank">Для сервисных центров</a></li>
                            <li class="nav-item"><a class="nav-link text-body-secondary" href="#">Вакансии</a></li>
                            <li class="nav-item"><a class="nav-link text-body-secondary" href="#">Команда</a></li>
                        </ul>
                    </div>

                    <!-- Link block -->
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <h5 class="text-white mb-2 mb-md-4">Популярные услуги</h5>
                        <ul class="nav flex-column text-primary-hover">
                            <li class="nav-item"><a class="nav-link text-body-secondary" href="/request">Тех. обслуживание</a></li>
                            <li class="nav-item"><a class="nav-link text-body-secondary" href="/request">Тормозные колодки</a></li>
                            <li class="nav-item"><a class="nav-link text-body-secondary" href="/request">Двигатели</a></li>
                            <li class="nav-item"><a class="nav-link text-body-secondary" href="/request">КПП</a></li>
                            <li class="nav-item"><a class="nav-link text-body-secondary" href="/request">Все услуги</a></li>
                        </ul>
                    </div>

                    <!-- Link block -->
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <h5 class="text-white mb-2 mb-md-4">Помощь и поддержка</h5>
                        <ul class="nav flex-column text-primary-hover">
                            <li class="nav-item"><a class="nav-link text-body-secondary" href="{{ Request::is('/') ? '#how-it-works' : '/#how-it-works' }}">Как это работает</a></li>
                            <li class="nav-item"><a class="nav-link text-body-secondary" href="https://api.whatsapp.com/send?phone=77772448909">Служба поддержки</a></li>
                            <li class="nav-item"><a class="nav-link text-body-secondary" href="https://api.whatsapp.com/send?phone=77772448909">Контакты</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Widget 2 END -->
        </div><!-- Row END -->

        <!-- Payment and card -->
        <div class="row justify-content-between mt-5">

            <!-- Payment card -->
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
                <h5 class="text-white mb-2">Способы оплаты</h5>
                <ul class="list-inline mb-0 mt-3">
                    <li class="list-inline-item"> <a href="#"><img src="{{ asset('img/payment/visa.svg') }}" height="15" alt="Visa"></a></li>
                    <li class="list-inline-item"> <a href="#"><img src="{{ asset('img/payment/mastercard.svg') }}" height="25" alt="Mastercard"></a></li>
                    <li class="list-inline-item"> <a href="#"><img src="{{ asset('img/payment/kaspi.svg') }}" height="25" alt="Kaspi"></a></li>
                </ul>
            </div>

            <!-- Social media icon -->
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3 text-sm-end">
                <h5 class="text-white mb-2">Мы в соц. сетях</h5>
                <ul class="list-inline mb-0 mt-3">
                    <li class="list-inline-item"> <a class="btn btn-sm px-2 mb-0" href="https://instagram.com/recar.kz" target="_blank"><i class="bi bi-instagram"></i></a> </li>
                    <li class="list-inline-item"> <a class="btn btn-sm px-2 mb-0" href="https://recar_kz.t.me" target="_blank"><i class="bi bi-telegram"></i></a> </li>
                    <li class="list-inline-item"> <a class="btn btn-sm px-2 mb-0" href="https://api.whatsapp.com/send?phone=77772448909" target="_blank"><i class="bi bi-whatsapp"></i></a> </li>
                    <li class="list-inline-item"> <a class="btn btn-sm px-2 mb-0" href="https://www.linkedin.com/company/recarkz/" target="_blank"><i class="bi bi-linkedin"></i></a> </li>
                </ul>
            </div>
        </div>

        <!-- Divider -->
        <hr class="mt-4 mb-0">

        <!-- Bottom footer -->
        <div class="row">
            <div class="container">
                <div class="d-lg-flex justify-content-between align-items-center py-3 text-center text-lg-start">
                    <!-- copyright text -->
                    <div class="text-body-secondary text-primary-hover">© 2023 Recar.kz. «2fix Group» LLP.</div>
                    <!-- copyright links-->
{{--                    <div class="nav mt-2 mt-lg-0">--}}
{{--                        <ul class="list-inline text-primary-hover mx-auto mb-0">--}}
{{--                            <li class="list-inline-item me-0"><a class="nav-link text-body-secondary py-1" href="#">Privacy policy</a></li>--}}
{{--                            <li class="list-inline-item me-0"><a class="nav-link text-body-secondary py-1" href="#">Terms and conditions</a></li>--}}
{{--                            <li class="list-inline-item me-0"><a class="nav-link text-body-secondary py-1 pe-0" href="#">Refund policy</a></li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</footer>