@extends('layout.app')

@section('content')
    <main class="flex-shrink-0" id="main">
        <section class="pt-md-11 pt-0 pb-5 mt-0 mb-5">
            <div class="container pb-5">
                <div class="row align-items-center">
                    <div class="col-12 col-md-5 col-lg-6 order-md-2 mb-5">
                        <!-- Header profile picture-->
                        <div class="d-flex justify-content-center mt-md-5 mt-lg-5 mt-xxl-0">
                            <img class="img-fluid mw-md-150 mw-lg-130 mb-6 mb-md-0 hidden-xs" src="{{ asset('/img/main-bg.svg') }}" alt="Recar.kz">
                            <img class="img-fluid mw-md-150 mw-lg-130 mb-6 mb-md-0 hidden-md hidden-lg" src="{{ asset('/img/bg-mini.png') }}" alt="Recar.kz">
                        </div>
                    </div>
                    <div class="col-12 col-md-7 col-lg-6 order-md-1 aos-init aos-animate">
                        <!-- Header text content-->
                        <div class="text-center text-md-start">
                            <h1 class="display-4 main-h1 fw-bolder mb-lg-5 mb-md-5"><span class="text-gradient d-inline">Сами заберем авто, починим и привезем</span></h1>
                        </div>
                        <div class="my-xs-3 text-center text-lg-start text-md-start">
                            <p>Лучшие механики, гарантия на работы, обслуживание на следующий день.</p>
                        </div>
                        <div class="text-center text-md-start">
                            <a class="w-xs-100 w-md-50 btn btn-dark shadow-sm px-5 fs-6 rounded-5 fw-normal text-uppercase" style="padding:13px 0;" href="/request">Заявка на выезд</a>
                            <a href="#how-it-works" class="px-5 w-xs-100 btn rounded-5 border ms-lg-2 btn-light mt-xs-3" style="padding:13px 0;">Как это работает?</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- About Section-->
        <section class="bg-gradient-primary-to-secondary text-white py-5">
            <div class="container">
                <div class="row my-3">
                    <div class="col-xxl-6 py-4">
                        <h2 class="fw-bolder">Recar.kz — ваша машина сама чинится, пока вы заняты делами</h2>
                    </div>
                </div>
                <div class="row my-5 justify-content-center">
                    <div class="col-xxl-4">
                        <div class="my-lg-5">
                            <h3 class="fw-bolder fst-italic"><span class="d-inline">Экономьте свое время</span></h3>
                            <p class="lead fw-light mb-4">Вы экономите свое время, пока мы заняты ремонтом вашего авто.</p>
                        </div>
                    </div>
                    <div class="col-xxl-4">
                        <div class="my-lg-5">
                            <h3 class="fw-bolder fst-italic"><span class="d-inline">Без стресса</span></h3>
                            <p class="lead fw-light mb-4">Вам больше не нужно разбираться в сложном процессе ремонта</p>
                        </div>
                    </div>
                    <div class="col-xxl-4">
                        <div class="my-lg-5">
                            <h3 class="fw-bolder fst-italic"><span class="d-inline">Без страхов</span></h3>
                            <p class="lead fw-light mb-4">У нас лучшие автосервисы. У нас гарантия. У вас уверенность.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="bg-white py-5" id="how-it-works">
            <div class="container">
                <h2 class="display-5 fw-bolder mb-5 text-center"><span class="text-gradient">Как это работает?</span></h2>
                <div class="row justify-content-evenly align-items-baseline text-center mb-5">
                    <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 mb-3">
                        <object type="image/svg+xml" width="80%" data="{{ asset('/img/steps/step1.svg') }}"></object>
                        <div class="text-center">
                            <span class="display-5 fw-bolder text-gradient">1</span>
                            <h4 class="fw-bold d-block">Забор авто</h4>
                            <span class="d-block">К вам приезжает наш водитель</span>
                            <span class="d-block">Получает краткую информацию</span>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 mb-3">
                        <object type="image/svg+xml" width="80%" data="{{ asset('/img/steps/step2.svg') }}"></object>
                        <div class="text-center">
                            <span class="display-5 fw-bolder text-gradient">2</span>
                            <h4 class="fw-bold d-block">Выбор автосервиса</h4>
                            <span class="d-block">Водитель отвозит авто в один из наших сертифицированных СТО.</span>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 mb-3">
                        <object type="image/svg+xml" width="80%" data="{{ asset('/img/steps/step3.svg') }}"></object>
                        <div class="text-center">
                            <span class="display-5 fw-bolder text-gradient">3</span>
                            <h4 class="fw-bold d-block">Диагностика</h4>
                            <span class="d-block">Отправляем вам список работ на утверждение</span>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-evenly align-items-baseline">
                    <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 mb-3">
                        <object type="image/svg+xml" width="80%" data="{{ asset('/img/steps/step4.svg') }}"></object>
                        <div class="text-center">
                            <span class="display-5 fw-bolder text-gradient">4</span>
                            <h4 class="fw-bold d-block">Ремонт</h4>
                            <span class="d-block">Наш партнер производит ремонт вашего авто</span>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 mb-3">
                        <object type="image/svg+xml" width="80%" data="{{ asset('/img/steps/step5.svg') }}"></object>
                        <div class="text-center">
                            <span class="display-5 fw-bolder text-gradient">5</span>
                            <h4 class="fw-bold d-block">Готово!</h4>
                            <span class="d-block">Ремонт завершен</span>
                            <span class="d-block">Мы привозим ваш автомобиль</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="py-5 bg-gradient-primary-to-secondary text-white">
            <div class="container my-5">
                <div class="text-center">
                    <h2 class="display-5 fw-bolder mb-5">Попробуйте, это легко!</h2>
                    <a class="btn btn-light btn-lg px-5 py-3 fs-6 fw-bolder" href="/request">Заказать водителя</a>
                </div>
            </div>
        </section>
        <section class="py-5" id="faq">
            <div class="container my-5">
                <h2 class="display-5 fw-bolder mb-5 text-center"><span class="text-gradient">Часто задаваемые вопросы</span></h2>
                <div class="faq-body row justify-content-center">
                    <div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
                        <div class="accordion accordion-flush" id="accordionFlushExample">
                            <div class="accordion-item">
                                <h2 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse2" aria-expanded="false" aria-controls="flush-collapse2">
                                        Что будет, если водитель поцарапает или повредит машину?
                                    </button>
                                </h2>
                                <div id="flush-collapse2" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                        Мы понимаем ваши опасения и заботимся о безопасности вашего автомобиля.
                                        Все наши партнеры прошли тщательную проверку и обладают необходимыми страховками.
                                        В случае любого повреждения или непредвиденной ситуации во время оказания услуг,
                                        мы гарантируем полное возмещение убытков или ремонт повреждений на наш счет.
                                        Наша цель — обеспечить вам максимальное спокойствие и доверие.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse3" aria-expanded="false" aria-controls="flush-collapse3">
                                        Я волнуюсь за свои ценные вещи в машине, не украдут ли их?
                                    </button>
                                </h2>
                                <div id="flush-collapse3" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                        Мы полностью понимаем ваши опасения относительно ценных вещей.
                                        Каждый водитель и партнер-сервис, с которым мы работаем, проходит тщательный отбор
                                        и проверку перед тем, как начать сотрудничество с нами. Однако мы настоятельно
                                        рекомендуем вам не оставлять ценные вещи в автомобиле во время оказания услуги.
                                        Наша цель — обеспечить вам комфорт и безопасность на всех этапах сотрудничества.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse4" aria-expanded="false" aria-controls="flush-collapse4">
                                        Какие автосервисы будут чинить мою машину?
                                    </button>
                                </h2>
                                <div id="flush-collapse4" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                        Сертифицированные партнеры - это автосервисы, избранные нашей командой и которые прошли тесты на качество.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse5" aria-expanded="false" aria-controls="flush-collapse5">
                                        Могу ли я сам выбрать автосервис?
                                    </button>
                                </h2>
                                <div id="flush-collapse5" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                        Клиент всегда может выбрать свой автосервис, однако если он не входит в список наших проверенных партнеров, то мы не даем гарантию на их работы.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse6" aria-expanded="false" aria-controls="flush-collapse6">
                                        Сколько времени займёт ремонт или обслуживание моего автомобиля?
                                    </button>
                                </h2>
                                <div id="flush-collapse6" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                        Время ремонта или обслуживания может варьироваться в зависимости от сложности работы.
                                        Обычно мы предоставляем приблизительное время завершения работы после диагностики вашего автомобиля.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse7" aria-expanded="false" aria-controls="flush-collapse7">
                                        Как я могу отслеживать статус ремонта или обслуживания своего автомобиля?
                                    </button>
                                </h2>
                                <div id="flush-collapse7" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                        Мы уведомляем вас по SMS или сообщениям в WhatsApp о каждом этапе работы.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse8" aria-expanded="false" aria-controls="flush-collapse8">
                                        Могу ли я отменить заказ после того, как машину уже забрали?
                                    </button>
                                </h2>
                                <div id="flush-collapse8" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                        Да, вы можете отменить или перенести заказ. Однако, если уже проведена
                                        диагностика, то отмена будет платной - вам придется заплатить за диагностику.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse9" aria-expanded="false" aria-controls="flush-collapse9">
                                        Что делать, если я не удовлетворен качеством предоставленной услуги?
                                    </button>
                                </h2>
                                <div id="flush-collapse9" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                        В первую очередь свяжитесь с нами. Мы ценим отзывы наших клиентов и
                                        готовы рассмотреть каждую ситуацию индивидуально.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse10" aria-expanded="false" aria-controls="flush-collapse10">
                                        Будет ли моя машина страховаться во время транспортировки и ремонта?
                                    </button>
                                </h2>
                                <div id="flush-collapse10" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                        Да, ваш автомобиль страхуется на период транспортировки и на время ремонта в мастерской.
                                        Также, в случае каких-то происшествий не по вине водителя, будет задействована ваша личная страховка.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
