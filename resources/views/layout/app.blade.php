<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title }}</title>
    <meta name="description" content="{{ $desc }}">
    <link rel="icon" type="image/png" href="{{ asset('/img/favicon.png') }}">

    <!-- Facebook Meta Tags -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ url()->to('/') }}">
    <meta property="og:title" content="Recar.kz — ремонт авто не выходя из дома">
    <meta property="og:description" content="Recar.kz — сервис доставки авто в ваш любимый СТО. Мы сами забираем вашу машину, чиним в лучшем сервисном центре и привозим прямо к вам домой. Без стресса, без хлопот.">
    <meta property="og:image" content="{{ asset('/img/recar-og.png') }}">

    <!-- Twitter Meta Tags -->
    <meta name="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="{{ url()->to('/') }}">
    <meta name="twitter:title" content="Recar.kz — ремонт авто не выходя из дома">
    <meta name="twitter:description" content="Recar.kz — сервис доставки авто в ваш любимый СТО. Мы сами забираем вашу машину, чиним в лучшем сервисном центре и привозим прямо к вам домой. Без стресса, без хлопот.">
    <meta name="twitter:image" content="https://recar.kz/img/recar-og.png">

    <meta name="google-site-verification" content="OBaInxefWZJFqUanY-Kt1vxnZsprBqUtuqq_i-oqH-Q" />
    <meta name="yandex-verification" content="bb73ca2ab9b0796c" />

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">

    @vite(['resources/scss/app.scss', 'resources/js/app.js'])
</head>
<body class="d-flex flex-column h-100">
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light fixed-top bg-white py-lg-3 py-md-3">
    <div class="container pl-5">
        <a class="navbar-brand" href="/"><img src="{{ asset('/img/logo.png') }}" alt="Recar.kz logo" width="200"></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0 fw-bolder">
                <li class="nav-item"><a class="nav-link" href="/">Главная</a></li>
                <li class="nav-item"><a class="nav-link" href="{{ Request::is('/') ? '#how-it-works' : '/#how-it-works' }}">Как это работает?</a></li>
                <li class="nav-item"><a class="nav-link" href="{{ Request::is('/') ? '#faq' : '/#faq' }}">FAQ</a></li>
                <li class="nav-item"><a class="nav-link" href="https://forms.gle/1vtXbWkymAycXcNj7" target="_blank">Стать партнером</a></li>
            </ul>
        </div>
    </div>
</nav>
@yield('content')
@include('parts.footer')
@yield('script')
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();
        for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
        k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(95394281, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/95394281" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>
</html>
