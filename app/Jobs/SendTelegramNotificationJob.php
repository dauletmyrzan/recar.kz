<?php
namespace App\Jobs;

use App\Models\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Telegram\Bot\Laravel\Facades\Telegram;

class SendTelegramNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Request $request;

    protected bool $result;

    /**
     * Create a new job instance.
     */
    public function __construct(Request $request, bool $result)
    {
        $this->request = $request;
        $this->result  = $result;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $message = $this->result
            ? "Пришла новая заявка!\n" . implode("\n", $this->request->toArray())
            : 'Ошибка создания заявки';
        $chatId  = '195073147';

        Telegram::sendMessage([
            'chat_id' => $chatId,
            'text'    => $message,
        ]);
    }
}
