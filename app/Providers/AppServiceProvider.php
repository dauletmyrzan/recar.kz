<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $title = 'Recar.kz — ремонт авто не выходя из дома';
        $desc  = 'Recar.kz — сервис доставки авто в ваш любимый СТО. Мы сами забираем вашу машину, чиним в лучшем ' .
            'сервисном центре и привозим прямо к вам домой. Без стресса, без хлопот.';

        View::share([
            'googleFormLink' => 'https://forms.gle/1vtXbWkymAycXcNj7',
            'title'          => $title,
            'desc'           => $desc,
        ]);
    }
}
