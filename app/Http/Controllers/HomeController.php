<?php
namespace App\Http\Controllers;

use App\Services\SaveRequest\DTO;
use App\Services\SaveRequest\Service;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
    public function welcome(): View
    {
        $driversCount = $this->getRandomNumber();

        return view('welcome', compact('driversCount'));
    }

    public function requestView(): View
    {
        return view('request');
    }

    public function processRequest(Request $request)
    {
        $request->validate([
            'name'                 => ['required', 'string', 'regex:/^[a-zA-Zа-яА-Я\s-]+$/u', 'max:255'],
            'phone'                => ['required', 'string', 'regex:/^[0-9\s\+]+$/'],
            'address'              => 'required|string|max:255',
            'car'                  => 'required|string|max:255',
            'problem'              => 'required|string|max:255',
            'g-recaptcha-response' => 'required|string',
        ]);

        $recaptchaSecret   = '6Lfy99woAAAAAOcDOth-9s2lSXdMKWe_KQcveUbR';
        $recaptchaResponse = $request->input('g-recaptcha-response');
        $response          = Http::asForm()->post('https://www.google.com/recaptcha/api/siteverify', [
            'secret'   => $recaptchaSecret,
            'response' => $recaptchaResponse,
        ]);
        $recaptchaData     = $response->json();

        if (!$recaptchaData['success']) {
            return redirect()->back()->withErrors(['reCAPTCHA' => 'Подтвердите, что вы не робот.'])->withInput();
        }

        $service = new Service();
        $result  = $service->execute(new DTO(
            $request->get('name'),
            $request->get('phone'),
            $request->get('address'),
            $request->get('car'),
            $request->get('problem'),
        ));

        return redirect()->back()->with('success', $result);
    }

    private function getRandomNumber()
    {
        return Cache::remember('random_number', 7200, function () {
            return mt_rand(4, 15);
        });
    }
}
