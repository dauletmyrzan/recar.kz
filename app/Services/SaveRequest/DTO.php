<?php
namespace App\Services\SaveRequest;

class DTO
{
    public string $name;

    public string $phone;

    public string $address;

    public string $car;

    public ?string $problem;

    public function __construct(string $name, string $phone, string $address, string $car, ?string $problem)
    {
        $this->name    = $name;
        $this->phone   = $phone;
        $this->address = $address;
        $this->car     = $car;
        $this->problem = $problem;
    }
}
