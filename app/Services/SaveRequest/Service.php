<?php
namespace App\Services\SaveRequest;

use App\Jobs\SendTelegramNotificationJob;
use App\Models\Request;

final class Service
{
    public function execute(DTO $dto): bool
    {
        $request = new Request();

        $request->fill([
            'name'    => $dto->name,
            'phone'   => $dto->phone,
            'address' => $dto->address,
            'car'     => $dto->car,
            'problem' => $dto->problem,
        ]);

        $result = $request->save();

        dispatch(new SendTelegramNotificationJob($request, $result));

        return $result;
    }
}
